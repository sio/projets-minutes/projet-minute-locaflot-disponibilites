SET ROLE enseignants;
SET client_encoding = 'UTF8';
DROP SCHEMA "locaflot_dispo" CASCADE;

CREATE SCHEMA "locaflot_dispo";
CREATE TABLE "locaflot_dispo"."contrat_location" (
    "id" integer NOT NULL,
    "date" "date",
    "heure_debut" time without time zone,
    "heure_fin" time without time zone
);
CREATE TABLE "locaflot_dispo"."embarcation" (
    "id" character varying(4) NOT NULL,
    "couleur" character varying(15),
    "disponible" boolean,
    "type" character varying(2)
);
CREATE TABLE "locaflot_dispo"."louer" (
    "id_contrat" integer NOT NULL,
    "id_embarcation" character varying(4) NOT NULL,
    "nb_personnes" integer
);
CREATE TABLE "locaflot_dispo"."type_embarcation" (
    "code" character varying(2) NOT NULL,
    "nom" character varying(20),
    "nb_place" integer,
    "prix_demi_heure" numeric(5,2),
    "prix_heure" numeric(5,2),
    "prix_demi_jour" numeric(5,2),
    "prix_jour" numeric(5,2)
);
