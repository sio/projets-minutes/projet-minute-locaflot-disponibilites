GRANT USAGE ON SCHEMA "locaflot_dispo" TO "etudiants-slam";
GRANT SELECT ON TABLE "locaflot_dispo"."contrat_location" TO "etudiants-slam";
GRANT SELECT ON TABLE "locaflot_dispo"."embarcation" TO "etudiants-slam";
GRANT SELECT ON TABLE "locaflot_dispo"."louer" TO "etudiants-slam";
GRANT SELECT ON TABLE "locaflot_dispo"."type_embarcation" TO "etudiants-slam";
